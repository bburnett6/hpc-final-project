#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>

class Particle
{
public:

	double x;
	double y;
	double m;

	Particle(double init_x, double init_y, double init_vx, double init_vy, double init_m)
	{
		x = init_x;
		y = init_y;
		vx = init_vx;
		vy = init_vy;
		m = init_m;
		fx = 0.0;
		fy = 0.0;
	}

	void calc_force(Particle p2);
	void update(double dt);
private:

	double vx;
	double vy;
	double fx;
	double fy;
};

void Particle::calc_force(Particle p2)
{
	double g = 6.67408 * pow(10, -11);
	double dx = p2.x - x;
	double dy = p2.y - y;
	double r = sqrt(dx * dx + dy * dy);
	double f = g * m * p2.m / (r * r);

	//sum of the forces is the total force
	fx -= f * dx / r;
	fy -= f * dy / r;
}

void Particle::update(double dt)
{
	double ax = fx / m;
	double ay = fy / m;

	//update the velocity and position
	vx += 0.5 * dt * ax;
	vy += 0.5 * dt * ay;
	x += dt * vx;
	y += dt * vy;

	//reset the force to zero
	fx = 0.0;
	fy = 0.0;
}

double rd(unsigned int *seed, double min, double max)
{
	return ((double) rand_r(seed)) / RAND_MAX * (max - min) - (double) max;
}

int main(int argc, char **argv)
{
	if(argc < 4 || argc > 5)
	{
		printf("Usage: ./nbody num_particles num_timesteps num_threads\n");
		exit(0);
	}
	int numP = atoi(argv[1]);
	unsigned int seed = time(NULL); //seed for random position/velocity 
	Particle *ps[numP]; //an array of pointers so I can call the constructor
	double min_dim = - 2.5 * pow(10, 11);
	double max_dim = 2.5 * pow(10, 11);
	double min_mass = pow(10, 20);
	double max_mass = 2 * pow(10, 30);
	double dt = 10.0;
	long timesteps = atol(argv[2]);

	//omp stuff
	int num_t = atoi(argv[3]);
	double startT = omp_get_wtime();
	omp_set_dynamic(0);

	//initialize particles
	//random init
	for (int i = 0; i < numP; i++)
		ps[i] = new Particle(rd(&seed, min_dim, max_dim), rd(&seed, min_dim, max_dim), rd(&seed, -pow(10, 3), pow(10, 3)), rd(&seed, -pow(10, 3), pow(10, 3)), rd(&seed, min_mass, max_mass));
	
	/*
	//test data: the inner solar system
	numP = 5;
	ps[0] = new Particle(0.0, 0.0, 0.0, 0.0, 1.989 * pow(10, 30));
	ps[1] = new Particle(5.79 * pow(10, 10), 0.0, 0.0, 4.79 * pow(10, 4), 3.302 * pow(10, 23));
	ps[2] = new Particle(1.082 * pow(10, 11), 0.0, 0.0, 3.5 * pow(10, 4), 4.869 * pow(10, 24));
	ps[3] = new Particle(1.496 * pow(10, 11), 0.0, 0.0, 2.98 * pow(10, 4), 5.974 * pow(10, 24));
	ps[4] = new Particle(2.279 * pow(10, 11), 0.0, 0.0, 2.41 * pow(10, 4), 6.419 * pow(10, 23));
	*/

	#pragma omp parallel num_threads(num_t)
	for (long t = 0; t < timesteps; t++)
	{
		//calculate force on each particle
		#pragma omp for 
		for (int i = 0; i < numP; i++)
		{
			for (int j = 0; j < numP; j++)
			{
				if (i != j)
				{
					//printf("%d on %d, %d\n",omp_get_thread_num(), i, j);
					ps[i]->calc_force(*ps[j]);
				}
			}
		}
		#pragma omp barrier

		//update particles
		#pragma omp for
		for (int i = 0; i < numP; i++)
			ps[i]->update(dt);

	}

	printf("Run time: %0.3f\n", omp_get_wtime() - startT);
	//printf("x = %1.3e\n", ps[3]->x); //debug random sample

	return 0;
}