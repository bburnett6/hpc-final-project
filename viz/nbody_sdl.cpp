#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Particle
{
public:

	double x;
	double y;
	double m;

	Particle(double init_x, double init_y, double init_vx, double init_vy, double init_m)
	{
		x = init_x;
		y = init_y;
		vx = init_vx;
		vy = init_vy;
		m = init_m;
		fx = 0.0;
		fy = 0.0;
	}

	void calc_force(Particle p2);
	void update(double dt);
private:

	double vx;
	double vy;
	double fx;
	double fy;
};

void Particle::calc_force(Particle p2)
{
	double g = 6.67408 * pow(10, -11);
	double dx = p2.x - x;
	double dy = p2.y - y;
	double r = sqrt(dx * dx + dy * dy);
	double f = g * m * p2.m / (r * r);

	//sum of the forces is the total force
	fx -= f * dx / r;
	fy -= f * dy / r;
}

void Particle::update(double dt)
{
	double ax = fx / m;
	double ay = fy / m;

	//update the velocity and position
	vx += 0.5 * dt * ax;
	vy += 0.5 * dt * ay;
	x += dt * vx;
	y += dt * vy;

	//reset the force to zero
	fx = 0.0;
	fy = 0.0;
}

double rd(unsigned int *seed, double min, double max)
{
	return ((double) rand_r(seed)) / RAND_MAX * (max - min) - (double) max;
}

//SDL functions
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 640;
SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "GRAVITY", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

void close()
{
	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

//And finally... Main

int main()
{
	int numP = 50;
	unsigned int seed = time(NULL); //seed for random position/velocity 
	Particle *ps[numP];
	double min_dim = - 2.5 * pow(10, 11);
	double max_dim = 2.5 * pow(10, 11);
	double min_mass = pow(10, 10);
	double max_mass = 2 * pow(10, 30);
	double dt = 50.0;

	//initialize sdl
	bool quit = false;
	SDL_Event e;
	if (!init())
	{
		quit = true;
		printf("SDL initialization failed...\n");
	}

	//initialize particles
	//random init
	for (int i = 0; i < numP; i++)
		ps[i] = new Particle(rd(&seed, min_dim, max_dim), rd(&seed, min_dim, max_dim), rd(&seed, -pow(10, 3), pow(10, 3)), rd(&seed, -pow(10, 3), pow(10, 3)), rd(&seed, min_mass, max_mass));

	/*
	//test data
	numP = 5;
	ps[0] = new Particle(0.0, 0.0, 0.0, 0.0, 1.989 * pow(10, 30));
	ps[1] = new Particle(5.79 * pow(10, 10), 0.0, 0.0, 4.79 * pow(10, 4), 3.302 * pow(10, 23));
	ps[2] = new Particle(1.082 * pow(10, 11), 0.0, 0.0, 3.5 * pow(10, 4), 4.869 * pow(10, 24));
	ps[3] = new Particle(1.496 * pow(10, 11), 0.0, 0.0, 2.98 * pow(10, 4), 5.974 * pow(10, 24));
	ps[4] = new Particle(2.279 * pow(10, 11), 0.0, 0.0, 2.41 * pow(10, 4), 6.419 * pow(10, 23));
	*/

	while(!quit)
	{
		//printf("x = %1.3e\n", ps[4]->x);
		//calculate force on each particle
		for (int i = 0; i < numP; i++)
		{
			for (int j = 0; j < numP; j++)
			{
				if (i != j)
					ps[i]->calc_force(*ps[j]);
			}
		}

		//update particles
		for (int i = 0; i < numP; i++)
			ps[i]->update(dt);

		//draw particles
		while( SDL_PollEvent( &e ) != 0 )
		{
			if( e.type == SDL_QUIT )
				quit = true;
		}

		SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
		SDL_RenderClear( gRenderer );

		SDL_SetRenderDrawColor( gRenderer, 0xFF, 0x00, 0x00, 0xFF );
		for (int i = 0; i < numP; i++)
		{
			int x = (int) ((ps[i]->x + max_dim) / max_dim * (SCREEN_WIDTH / 2));
			int y = (int) ((max_dim - ps[i]->y) / max_dim * (SCREEN_HEIGHT / 2));
			int size = (int) (log(fabs(ps[i]->m)) * 0.1);
			SDL_Rect part = { x, y, size, size };		
			SDL_RenderFillRect( gRenderer, &part );
		}
		
		SDL_RenderPresent( gRenderer );
	}

	return 0;
}
