\documentclass{article}
\usepackage[margin=1in]{geometry} %1 in margins
\usepackage{graphicx} %For including graphics
\usepackage[labelfont=bf]{caption} %Make float labels bold
\usepackage{subcaption}
\usepackage{amsmath}

\renewcommand{\floatpagefraction}{0.95}
\renewcommand{\topfraction}{0.95}
\renewcommand{\textfraction}{0.05}

\usepackage{xcolor}
\usepackage{listings}

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
    backgroundcolor=\color{backgroundColour},   
    commentstyle=\color{mGreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{mGray},
    stringstyle=\color{mPurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    language=C
}
\usepackage{verbatim} %Allow verbatim input for code
\usepackage{float} %Defining the Program environment
\floatstyle{boxed}
\newfloat{program}{htbp}{pgm}
\floatname{program}{Program}

\begin{document}

\begin{center}
{\large {\bf HPC Final Project: The Barnes-Hut Algorithm}}\\

Ben Burnett

December 13, 2018
\end{center}

{\bf Introduction}

\vspace{\baselineskip}

One of the main goals of HPC is making computations that take a long time quicker. Methods of decreasing runtime have been a focus of researchers for decades now and there are a lot of neat and interesting results that have come from these efforts. At the forefront of these developments is of course parallelism. Being able to devide up a problem into many chunks and destributing that work between several computers is a beautiful solution, but occasionally it is not enough. In this project I attempted to explore the other options available for speeding up the runtime of a problem specifically for problems dealing with multiple bodies. This category of problem is called an n-body problem.

N-body problems occur in many different fields. They typically deal with calculating the interaction each body in a system has on each other. This calculation alone has a polynomial runtime associated with it and if you throw in a difficult interaction calculation on top of it, one should expect even worse performance. The question then becomes, is it possible to decrease the time spent calculating between each body? The answer is an estatic yes with multiple options! In this project I will be implementing and analyzing one of these options, known as the Barnse-Hut algorithm. In order to get a good understanding of the algorithm itself and what it can offer, I will be implementing the algorithm to solve Newton's Law of Gravitation. I will also attempt to parallelize the algorithm and contrast that against parallelizing the no-algorithm solution. 

In this report I will provide a some initial background on the Barns-Hut algorithm and what benefits it provides as well as details on my implementation of the algorithm. Following this, I will show the results of what my implementation provided and a commentary on other methods.

\vspace{\baselineskip}

{\bf Background}

\vspace{\baselineskip}

The Barnes-Hut algorithm is named after its original designers Josh Barnes and Piet Hut. The algorithm itself is actually an approximation to the original n-body problem through the use of a tree data structure. The algorithm approximates the original n-body problem by partitioning space recursively into quadrants (octants if in three dimensions) until each body in the system occupies its own quadrant. An example of this can be viewed in figure ~\ref{fig:quad}. The data structure in this case is called a quadtree (octtree if in three dimensions) and each body occupies a leaf of the tree. 


\begin{figure}[htb]
 \begin{center}
  \includegraphics[width=0.5\textwidth]{quadtree.png}
 \end{center}
 \caption{Example of a Barnes-Hut Quadtree}
 \label{fig:quad}
\end{figure}

The approximation that the algorithm makes is as follows. Rather than calculating the interaction between each particle individually, using the quadtree we calculate the interaction between a center of mass provided by the quadtree. To better understand this lets take an example again from figure ~\ref{fig:quad}. In the original simulation body $A$ would calculate the interaction between $B$, $C$, $D$, etc. In the Barnes-Hut simulation, body $A$ would only calculate the interaction of the center of mass body represented by $B$, $C$, and $D$, the center of mass particle represented by $E$, $F$, and $G$, and then finally $H$. So the Barnes-Hut saved us from calculating 7 interactions to only 3.

The use case for this algorithm is pretty far reaching seeing as all the algorithm is doing is approximating away some of the bodies in the system through a center of mass. So as long as the bodies being used can have some definition of a center of mass or a simulated body, the Barnes-Hut algorithm should be able to be implemented. Some examples of where it has been used is in Astrophysics simulations of galaxy creation, Biophysics simulations of protien interactions, fluid dynamics simulations where sections of the fluid are considered to be large particles, and much more. 

There are drawbacks to the Barnes-Hut algorithm that should be addressed. As with any approximation, there are numerical errors that occur through the use of the Barnes-Hut. There is a parameter within the algorithm that can be adjusted to tune the algorithms accuracy at the cost of runtime. The parameter in question is used to determine whether a center of mass body should be used in place of an actual body given by:

$$ \theta > \frac{s}{d} $$

In this equation $s$ is the size of the quadrant that the center of mass body is located, $d$ is the distance between the center of mass and the body the calculation is being performed on, and $\theta$ is the accuracy parameter. In an implementation of the algorithm the accuracy parameter is set to a value greater than zero. If set to zero, the Barnes-Hut degenerates to the no-algorithm case and the algorithm becomes useless. This means a value close to zero is more accurate but more bodies will be considered thus causing the algorithm to take longer. 

There are also better algorithms than the Barnes-Hut. Using no algorithm has a $O(n^2)$ performance. The Barnes-Hut out performs this with a performance of $O(nlogn)$. But there is a method to do even better and get a performance of $O(n)$. That method is the Fast Multipole Method (FMM). This method in combination with a data structure based approximation uses a numerical approximation as well. This method can be slightly more involved to implement but anything with a linear runtime would be well worth the investigation.

\vspace{\baselineskip}

{\bf Work Done}

\vspace{\baselineskip}

My implementation of the Barnes-Hut algorithm was done using Newton's Law of Gravitation serving as the interaction. This law is given by:

$$ F = \frac{G m_1 m_2}{r_{12}} $$

I decided to use this for my interaction for a couple reasons. First, there is a lot of documentation about this law on the internet. Rather than dividing my research efforts into two categories, algorithm implementation and interaction implementation, I could instead focus on one new thing to learn. The second reason is that this interaction is a single equation and can be performed in constant time. This means I will be able to cleanly see the performance of the Barnes-Hut algorithm and not have to take into account any other factors.

In order to implement the Barnes-Hut algorithm, I first did implemented the no algorithm (hereafter called the brute force). I implemented this in C++ so that I could use an object oriented approach to solving this problem. This allowed me to set up the data structure I called a {\it Particle} that I could eventually put into my Barnes-Hut tree. Inside this particle data structure I was able to implement my interaction calculation so that I could easily transition to implementing the quadtree. This brute force implementation was fairly straight forward and had psuedo-code that looks like the following:

\begin{verbatim}
 for each particle i in system
    for each particle j in system
        calculate force between particle i and j
\end{verbatim}

This implementation also had the beneficial side effect of being embarrasingly parallel. So, using OpenMP, I also created a parallel brute force implementation.

From this point I was able to implement the Barnes-Hut algorithm. In order to do this I created a quadrant data structure called {\it Quad} and a quadtree data structure called {\it BHT} (Barnes-Hut Tree). These data structures are actually where a lot of the work of performing the algorithm is done. The creation and travesal of the tree is all done here allowing the psuedo-code for main to be:

\begin{verbatim}
 for each particle in system
    place particle in BHT
 
 for each particle in system
    calculate force on force using BHT
\end{verbatim}

The first for loop is fairly simple and is performed in $O(n)$ time. The second for loop is where the work is done and takes $O(nlogn)$, $n$ time for the for loop itself and $logn$ time for the calculation via tree traversal.

The way this was implemented also provided some parallelism prospects. The first for loop is not thread safe (is there a thread safe tree insertion method?) but the second for loop is parallelizable. Calculating the force on each particle is an independent process and the tree is not updated until the next time step. This means the second for loop could be parallelized which is exactly what I did next.

The results of this work can be viewed in figure ~\ref{fig:results}. Before we get into the plot, here are the details of the test being run. The question being answered was: given a number of bodies how quickly can the algorithm perform 1000 time steps? The bodies in each system were randomly initialized (the importance of this will be discussed shortly). For the Barnes-Hut algorithms, the accuracy coefficient was set to $\theta = 0.5$. The computations were performed on a system with Intel Haswell CPUs and the number of threads used was kept at 16 for each parallel run. 

In the plot in figure ~\ref{fig:results}, we can see the expected polynomial time from the brute force implementation and then the fairly nice time save provided by the Barnes-Hut algorithm. The interesting results come from the parallel runs. Based off of the plots the parallel brute force actually out performs the parallel Barnes-Hut up to a certain point. I believe this is due to the overhead of having to create the quadtree in serial is holding the algorithm back. I would also guess that this result would be different if I were to implement a more costly interaction. Calculating the interaction is being performed in the part of the Barnes-Hut algorithm that is being done in parallel so if this part were more computationally difficult, the overhead of creating the tree will become negligible. Another thing to point out is the jitter in the parallel Barnes-Hut timing. This jitter is actually due to the random body initialization. The Barnes-Hut is sensitive to body placement and if all the bodies are too close together, then the algorithm varies in its performance.

\begin{figure}[htb]
 \begin{center}
  \includegraphics[width=0.5\textwidth]{simple_scaling.pdf}
 \end{center}
 \caption{Results of the various methods}
 \label{fig:results}
\end{figure}

At this point you might be asking how I was sure I implemented things correctly? That is the final thing I worked on for this project. Things actually were not working until I worked on this final step. Thinking about the presentation for this project I actually decided to make a visual representation of my implementations. To do this I used the 2D graphics C library called SDL2. In order to compile and run my visualization, the SDL2 header files and shared objects have to be available on your system. On Ubuntu Linux systems this is as easy as installing the {\verb libsdl2-2.0 libsdl2-dev libsdl2-image-2.2.0 libsdl2-image-dev} packages. All I am doing in these visualizations is translating the xy plane that the particles live in into screen space and then plotting the particles and quadrants using the SDL2 geometry rendering utilities. Using this visualization I actually found a lot of bugs in my code. Considering the visualizations now do what I would expect based on the Physics, I can confidently say that things are working.

\vspace{\baselineskip}

{\bf Conclusion}

\vspace{\baselineskip}

Overall this was a really fun and insightful project to work on. Being able to explore these useful tools is always benificial and I hope to be able to look further into what is possible here. In my own research, I am looking at speeding up a code that has its own n-body problem so it has already been worth it to look into these methods of solving n-body problems.

There is also a lot of work that could be done still on this project. First off, I only performed a strong scaling test in this analysis. Seeing the weak scaling results would be interesting to look into as well. Second, comparing the Barnes-Hut to the FMM. I hope to go on and implement the FMM and see what it has to offer compared to the Barnes-Hut algorithm. 

\vspace{\baselineskip}

{\bf Resources Used}

\vspace{\baselineskip}

Zuniga, M. C. (n.d.). DYNAMIC PARALLELISM IN GPU OPTIMIZED BARNES HUT TREES FOR MOLECULAR DYNAMICS SIMULATIONS. Retrieved December 12, 2018, from

\noindent {\verb https://pdfs.semanticscholar.org/08b2/276b3653695cc00668aea6e19333a205ef8d.pdf }

\vspace{\baselineskip}

Barnes–Hut simulation. (2018, October 11). Retrieved from 

\noindent {\verb https://en.wikipedia.org/wiki/Barnes–Hut_simulation }

\vspace{\baselineskip}

COS 126 Programming Assignment: Barnes-Hut Galaxy Simulator. (n.d.). Retrieved from 

\noindent {\verb http://www.cs.princeton.edu/courses/archive/fall03/cs126/assignments/barnes-hut.html }



\end{document}
