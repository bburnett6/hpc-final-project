#!/bin/bash

g++ nbody.cpp -o nbody -fopenmp -lm
g++ nbody_omp.cpp -o nbody_omp -fopenmp -lm

