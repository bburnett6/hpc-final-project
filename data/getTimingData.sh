#!/bin/bash

for n in 4200 4400 4600 4800 5000
do
	echo "bodies = $n" >> nbody.dat
	../no_algo/nbody $n 1000 >> nbody.dat
	echo $n >> nbody_omp.dat
	../no_algo/nbody_omp $n 1000 16 >> nbody_omp.dat
	echo $n >> bhut.dat
	../barnes_hut/bhut $n 1000 >> bhut.dat
	echo $n >> bhut_omp.dat
	../barnes_hut/bhut_omp $n 1000 16 >> bhut_omp.dat
done
