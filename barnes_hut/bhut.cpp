#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>

//###########################
//Particle class
//###########################
class Particle
{
public:

	double x;
	double y;
	double m;
	int id; //id should be 0 or higher. Negative numbers are for COM particles

	Particle(double init_x, double init_y, double init_vx, double init_vy, double init_m, int init_id)
	{
		x = init_x;
		y = init_y;
		vx = init_vx;
		vy = init_vy;
		m = init_m;
		id = init_id;
		fx = 0.0;
		fy = 0.0;
	}

	void calc_force(Particle p2);
	void update(double dt);
	void printParticle();
private:

	double vx;
	double vy;
	double fx;
	double fy;
};

void Particle::calc_force(Particle p2)
{
	double g = 6.67408 * pow(10, -11);
	double dx = p2.x - x;
	double dy = p2.y - y;
	double r = sqrt(dx * dx + dy * dy);
	double f = g * m * p2.m / (r * r);

	//sum of the forces is the total force
	fx -= f * dx / r;
	fy -= f * dy / r;
}

void Particle::update(double dt)
{
	double ax = fx / m;
	double ay = fy / m;

	//update the velocity and position
	vx += 0.5 * dt * ax;
	vy += 0.5 * dt * ay;
	x += dt * vx;
	y += dt * vy;

	//reset the force to zero
	fx = 0.0;
	fy = 0.0;
}

void Particle::printParticle()
{
	printf("P: x = %1.2e, y = %1.2e, id = %i\n", x, y, id);
}

//###########################
//Quad class
//###########################

class Quad
{
public:
	Quad(double tl_ptx, double tl_pty, double init_l)
	{
		ptx = tl_ptx;
		pty = tl_pty;
		l = init_l;
	}

	double l; //the length of one of the sides of the quadrant
	double ptx; //the x coord of a point representing the top left corner of the quadrant
	double pty;

	bool contains(double x, double y);
	void printQuad();

	Quad* tl();
	Quad* tr();
	Quad* bl();
	Quad* br();

private:
 
};

bool Quad::contains(double x, double y)
{
	/*
	//DEBUG
	printf("%f, %f, %f\n", ptx, x, ptx + l);
	bool b1 = ptx < x;
	bool b2 = x < ptx + l;
	bool b3 = pty - l <= y;
	bool b4 = y < pty;

	if (!b1)
		printf("failed 1\n");
	if (!b2)
		printf("failed 2\n");
	if (!b3)
		printf("failed 3\n");
	if (!b4)
		printf("failed 4\n");
	*/
	return (this->ptx <= x && x <= this->ptx + l && this->pty - l <= y && y <= this->pty);
}

void Quad::printQuad()
{
	printf("Q: x = %1.2e, y = %1.2e, l = %1.2e\n", ptx, pty, l);
}

Quad* Quad::tl()
{
	return (new Quad(this->ptx, this->pty, l / 2));
}

Quad* Quad::tr()
{
	return(new Quad(this->ptx + l / 2, this->pty, l / 2));
}

Quad* Quad::bl()
{
	return(new Quad(this->ptx, this->pty - l / 2, l / 2));
}

Quad* Quad::br()
{
	return(new Quad(this->ptx + l / 2, this->pty - l / 2, l / 2));
}

//###########################
//BHT class
//###########################

class BHT
{
public:
	BHT(Quad *init_q, Particle *init_p)
	{
		q = init_q;
		p = init_p;
		tr = NULL;
		tl = NULL;
		br = NULL;
		bl = NULL;
	}

	Particle *p;
	Quad *q;

	void insert(Particle *p2);
	void calc_force(Particle *b);
	void printBHT(int level);
	void deleteTree();

private:
	BHT *tr; //top right
	BHT *tl; 
	BHT *br; //bottom right
	BHT *bl;
};

void BHT::insert(Particle *p2)
{
	double tot_mass = p->m + p2->m;
	double com_x = (p->x * p->m + p2->x * p2->m) / tot_mass;
	double com_y = (p->y * p->m + p2->y * p2->m) / tot_mass;
	Quad* trq = q->tr();
	Quad* tlq = q->tl();
	Quad* brq = q->br();
	Quad* blq = q->bl();

	//put p and p2 into their new quadrants
	if (p->id >= 0)
	{
		if (trq->contains(p->x, p->y))
		{
			//printf("inserting %d in tr\n", p->id);
			if (tr)
				tr->insert(p);
			else
				tr = new BHT(trq, p);
		}
		else if (tlq->contains(p->x, p->y))
		{
			//printf("inserting %d in tl\n", p->id);
			if (tl)
				tl->insert(p);
			else
				tl = new BHT(tlq, p);
		}
		else if (brq->contains(p->x, p->y))
		{
			//printf("inserting %d in br\n", p->id);
			if (br)
				br->insert(p);
			else
				br = new BHT(brq, p);
		}
		else if (blq->contains(p->x, p->y))
		{
			//printf("inserting %d in bl\n", p->id);
			if (bl)
				bl->insert(p);
			else
				bl = new BHT(blq, p);
		}
		else
		{
			tlq->printQuad();
			trq->printQuad();
			blq->printQuad();
			brq->printQuad();
			p->printParticle();
			printf("error on inserting p %d\n", p->id);
		}
	}

	if (p2->id >= 0)
	{
		if (trq->contains(p2->x, p2->y))
		{
			//printf("inserting %d in tr\n", p2->id);
			if (tr)
				tr->insert(p2);
			else
				tr = new BHT(trq, p2);
		}
		else if (tlq->contains(p2->x, p2->y))
		{
			//printf("inserting %d in tl\n", p2->id);
			if (tl)
				tl->insert(p2);
			else
				tl = new BHT(tlq, p2);
		}
		else if (brq->contains(p2->x, p2->y))
		{
			//printf("inserting %d in br\n", p2->id);
			if (br)
				br->insert(p2);
			else
				br = new BHT(brq, p2);
		}
		else if (blq->contains(p2->x, p2->y))
		{
			//printf("inserting %d in bl\n", p2->id);
			if (bl)
				bl->insert(p2);
			else
				bl = new BHT(blq, p2);
		}
		else
		{
			tlq->printQuad();
			trq->printQuad();
			blq->printQuad();
			brq->printQuad();
			p2->printParticle();
			printf("error on inserting p2 %d\n", p2->id);
		}
	}

	//update the particle in this node to the com
	//no velocity needed here either
	p = new Particle(com_x, com_y, 0.0, 0.0, tot_mass, -1); //-1 is the COM particle
}

void BHT::calc_force(Particle *b)
{
	double theta = 0.5; //threshold for determining if the COM is sufficiently far away for approximation

	if (q->l / sqrt((b->x - p->x) * (b->x - p->x) + (b->y - p->y) * (b->y - p->y)) < theta)
	{
		if(p->id != b->id)
			b->calc_force(*p);
	}
	else 
	{
		if (tr)
			tr->calc_force(b);
		if (tl)
			tl->calc_force(b);
		if (br)
			br->calc_force(b);
		if (bl)
			bl->calc_force(b);
	}
}

void BHT::printBHT(int level)
{
	printf("Level %d: id: %d\n", level, p->id);
	if (tr)
		tr->printBHT(level+1);
	if (tl)
		tl->printBHT(level+1);
	if (br)
		br->printBHT(level+1);
	if (bl)
		bl->printBHT(level+1);
}

void BHT::deleteTree()
{
	if (tr)
	{
		tr->deleteTree();
		delete tr;
	}
	if (tl)
	{
		tl->deleteTree();
		delete tl;
	}
	if (br)
	{
		br->deleteTree();
		delete br;
	}
	if (bl)
	{
		bl->deleteTree();
		delete bl;
	}

	if (p->id == -1)
		delete p;
	delete q;
}

//###########################
//Other functions
//###########################

double rd(unsigned int *seed, double min, double max)
{
	return ((double) rand_r(seed)) / RAND_MAX * (max - min) - (double) max;
}

//###########################
//Main
//###########################

int main(int argc, char **argv)
{
	if(argc < 3 || argc > 4)
	{
		printf("Usage: ./nbody num_particles num_timesteps\n");
		exit(0);
	}
	int numP = atoi(argv[1]);
	unsigned int seed = 106; //seed for random position/velocity 
	Particle *ps[numP]; //an array of pointers so I can call the constructor
	double min_dim = - 2.5 * pow(10, 11);
	double max_dim = 2.5 * pow(10, 11);
	double min_mass = pow(10, 10);
	double max_mass = 2 * pow(10, 20);
	double dt = 10.0;
	long timesteps = atol(argv[2]);
	double start_t = omp_get_wtime();

	//initialize particles
	//random init
	for (int i = 0; i < numP; i++)
		ps[i] = new Particle(rd(&seed, min_dim / 1000, max_dim / 1000), rd(&seed, min_dim / 1000, max_dim / 1000), 0.0, 0.0, rd(&seed, min_mass, max_mass), i);
	
	/*
	//test data: the inner solar system
	numP = 5;
	ps[0] = new Particle(0.0, 0.0, 0.0, 0.0, 1.989 * pow(10, 30), 0);
	ps[1] = new Particle(5.79 * pow(10, 10), 0.0, 0.0, 4.79 * pow(10, 4), 3.302 * pow(10, 23), 1);
	ps[2] = new Particle(1.082 * pow(10, 11), 0.0, 0.0, 3.5 * pow(10, 4), 4.869 * pow(10, 24), 2);
	ps[3] = new Particle(1.496 * pow(10, 11), 0.0, 0.0, 2.98 * pow(10, 4), 5.974 * pow(10, 24), 3);
	ps[4] = new Particle(2.279 * pow(10, 11), 0.0, 0.0, 2.41 * pow(10, 4), 6.419 * pow(10, 23), 4);
	*/

	for (long t = 0; t < timesteps; t++)
	{
		//construct the BHT
		//the head node is the entire square. This means at the end of the BHT
		//construction we will have the total COM in this node
		BHT head = BHT(new Quad(min_dim, max_dim, max_dim * 2), ps[0]);

		//NOTE FOR LATER PARALLELIZATION: BHT creation is not thread safe
		for (int i = 1; i < numP; i++)
		{
			head.insert(ps[i]);
		}

		//NOTE FOR LATER PARALLELIZATION: Calculating the force on each particle via
		//the BHT is thread safe :)
		for (int i = 0; i < numP; i++)
			head.calc_force(ps[i]);

		for (int i = 0; i < numP; i++)
			ps[i]->update(dt);

		head.deleteTree();
		//delete head;
	}

	//head.printBHT(0);
	//ps[3]->printParticle();
	printf("Run time: %0.3f\n", omp_get_wtime() - start_t);

	return 0;
}