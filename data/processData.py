from __future__ import division
import subprocess as sp
import matplotlib.pyplot as plt 
import numpy as np 

time_n = []
time_no = []
time_b = []
time_bo = []
num_bodies = []

with open("nbody.dat", 'r') as f:
	for line in f.readlines():
		if 'Run time:' in line:
			time_n.append(float(line.strip().split()[-1]))
		if 'bodies' in line:
			num_bodies.append(int(line.strip().split()[-1]))

with open("nbody_omp.dat", 'r') as f:
	for line in f.readlines():
		if 'Run time:' in line:
			time_no.append(float(line.strip().split()[-1]))

with open("bhut.dat", 'r') as f:
	for line in f.readlines():
		if 'Run time:' in line:
			time_b.append(float(line.strip().split()[-1]))

with open("bhut_omp.dat", 'r') as f:
	for line in f.readlines():
		if 'Run time:' in line:
			time_bo.append(float(line.strip().split()[-1]))

plt.figure()
plt.plot(num_bodies, time_n, '-', color='red', label='Brute Force Time')
plt.plot(num_bodies, time_no, '-', color='blue', label='Brute Force Parallel Time')
plt.plot(num_bodies, time_b, '-', color='green', label='Barnes-Hut Time')
plt.plot(num_bodies, time_bo, '-', color='orange', label='Barnes-Hut Parallel Time')
plt.xlabel('Number of Bodies')
plt.ylabel('Program Walltime (seconds)')
plt.xlim([num_bodies[0], num_bodies[-1]])
plt.ylim([0, 100])
plt.title('Simple Scaling Test of N-Body Algorithms')
plt.legend()
#plt.show()
plt.savefig('simple_scaling.pdf')